# README #

### Purpose of Project ###

* In this project I will attemp to restructure my maze that I have created
* from what is now a two dimensional array of node objects into a single linked
* list of Node objects

### Implementation Approach ###

* In my implementation, I will create a nested for loop of the x and y dimensions
* of the maze and insert a node at the nth element of the linked list.